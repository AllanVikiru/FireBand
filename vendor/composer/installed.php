<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-web',
    'version' => 'dev-web',
    'aliases' => 
    array (
    ),
    'reference' => '4e6d8b13d9da1bd76d9807ce78c570210bb7b798',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-web',
      'version' => 'dev-web',
      'aliases' => 
      array (
      ),
      'reference' => '4e6d8b13d9da1bd76d9807ce78c570210bb7b798',
    ),
    'delight-im/auth' => 
    array (
      'pretty_version' => 'v8.2.2',
      'version' => '8.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '729c76668fa638769ab834c904d50696686797e9',
    ),
    'delight-im/base64' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '687b2a49f663e162030a8d27b32838bbe7f91c78',
    ),
    'delight-im/cookie' => 
    array (
      'pretty_version' => 'v3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '67065d34272377d63bab0bd58f984f9b228c803f',
    ),
    'delight-im/db' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7d6f4c7a7e8ba6a297bfc30d65702479fd0b5f7c',
    ),
    'delight-im/http' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a19a72a7eac8b1301aa972fb20cff494ac43e09',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.2.0',
      'version' => '6.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e38888a75c070304ca5514197d4847a59a5c853f',
    ),
  ),
);
