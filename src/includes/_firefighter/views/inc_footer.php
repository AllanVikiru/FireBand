<?php
//footer for firefighter dashboard
?>
<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="text-center">
            <a class="font-w600" href="#"><?=$cb->name . ' ' . $cb->version?></a> &copy; <span class="js-year-copy"></span>&nbsp;,&nbsp;designed and developed by <a class="font-w600" href="https://github.com/AllanVikiru" target="_blank"><?=$cb->author?></a>
        </div>
</footer>
<!-- END Footer -->
