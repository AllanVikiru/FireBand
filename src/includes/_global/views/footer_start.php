<?php
// start of footer section - all vital JS scripts are included here
?>
<!-- Codebase JS -->
<script src="<?php echo $cb->assets_folder; ?>/js/codebase.core.min.js"></script>
<script src="<?php echo $cb->assets_folder; ?>/js/codebase.app.min.js"></script>
<!-- bootbox for confirm pop up -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
