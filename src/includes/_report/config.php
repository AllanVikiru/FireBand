<?php
/**
 * _report/config.php
 * 
 * Report Dashboard view configuration file
 * 
 */

// INCLUDED VIEWS
$cb->inc_header                 = 'src/includes/_report/views/inc_header.php';
$cb->inc_footer                 = 'src/includes/_report/views/inc_footer.php';

// HEADER SETTINGS
$cb->l_header_fixed     = true;
$cb->l_header_style     = 'glass-inverse';
