<?php //footer specific code for report dashboard footer ?>

<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-s clearfix text-center">
        <mark class="text-dark-op font-italic font-w400">Header Image Credits:
            <a class="link-effect" href="https://www.pexels.com//@tomfisk">Tom Fisk</a> from Pexels
        </mark>
    </div>
</footer>
<!-- END Footer -->